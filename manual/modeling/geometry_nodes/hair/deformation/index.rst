
##########################
  Hair Deformation Nodes
##########################

.. toctree::
   :maxdepth: 1

   blend_hair_curves.rst
   displace_hair_curves.rst
   frizz_hair_curves.rst
   hair_curves_noise.rst
   roll_hair_curves.rst
   rotate_hair_curves.rst
   shrinkwrap_hair_curves.rst
   smooth_hair_curves.rst
   straighten_hair_curves.rst
   trim_hair_curves.rst
