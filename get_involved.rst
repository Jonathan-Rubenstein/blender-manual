:orphan:

.. RST versions of the "Get Involved" pages on blender.org related to the documentation project.
   :: Copy this file into the /manual folder to build it (but watch out not to accidentally committing it).

################################
  Documentation -- blender.org
################################


.. _Get Involved: https://www.blender.org/get-involved/
.. _Documentation: https://www.blender.org/get-involved/documentation/

***************
`Get Involved`_
***************

`Documentation`_
================

A large number of people are helping with the
`Blender Manual <https://docs.blender.org/manual/en/dev/>`__
to organize and write our complete and freely available end-user documentation.

Volunteers interested in documentation translations can contact the team as well.

`Read more » <https://www.blender.org/get-involved/documenters>`__


****************
`Documentation`_
****************

About
=====

Blender's official documentation can be found in the online
`Blender Manual <https://docs.blender.org/manual/en/dev/>`__.
The Blender Manual is written using reStructuredText (RST) and
is built with `Sphinx <http://www.sphinx-doc.org/en/stable/>`__.

This project is run by a small team of volunteers and we would love your contributions!


How to Get Started
==================

#. `Install the manual locally <https://docs.blender.org/manual/en/dev/contribute/install/index.html>`__
#. `Build the manual locally <https://docs.blender.org/manual/en/dev/contribute/build/index.html>`__
#. Open an issue with your interest in helping


Communication
=============

- `#docs <https://blender.chat/channel/docs>`__ -- Chat with documenters, developers, and users.
- `Developer Forum <https://devtalk.blender.org/>`__
- `Issue tracker <https://projects.blender.org/blender/documentation>`__ -- Report bugs or
  find projects you can work on to improve the documentation.


Translations
============

The Blender manual is also being actively translated into a dozen languages.
If you would like to help to translate the manual from English to your language follow the above
"How to Get Started" guide but simply say you want to help with translations.

You should also read the following documents:

- `Translation contributing guide <https://docs.blender.org/manual/en/dev/contribute/translations/contribute.html>`__
- `Translation style guide <https://docs.blender.org/manual/en/dev/contribute/translations/style_guide.html>`__
